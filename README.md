# Site Master TRANS

Un thème WordPress pour [le site Master TRANS](http://head.hesge.ch/mastertrans/).

Il s'agit d'un thème enfant pour le thème "Libre", créé par Caroline Moore, webdesigner chez Automattic.

Plus d'infos sur Libre:

* https://wordpress.com/themes/libre/
* https://en.blog.wordpress.com/2015/07/02/libre/



