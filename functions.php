<?php

function trans_register_styles() {
	/**
	 * Custom CSS
	 */
	 
	 wp_enqueue_style( 
	 		'libre-orig-style', 
	 		get_template_directory_uri() . '/style.css', // main.css
	 		false, // dependencies
	 		null // version
	 );
	 
	 wp_enqueue_style( 
	 		'frm_fonts', 
	 		'https://head.hesge.ch/biblio/wp-content/plugins/formidable/css/frm_fonts.css', // main.css
	 		false, // dependencies
	 		'2.03.09' // version
	 );
	 
	 wp_enqueue_style( 
	 		'frm_dropzone', 
	 		'https://head.hesge.ch/biblio/wp-content/plugins/formidable/pro/css/dropzone.css', // main.css
	 		false, // dependencies
	 		'2.03.09' // version
	 );
	 
	
		
		/* Remove uneccessary fonts loaded by parent theme */
		
//		wp_dequeue_style( 'twentysixteen-style' );
//		wp_deregister_style( 'twentysixteen-style' );
//		
//		wp_dequeue_style( 'twentysixteen-fonts' );
//		wp_deregister_style( 'twentysixteen-fonts' );
}
add_action( 'wp_enqueue_scripts', 'trans_register_styles', 25 );


/**
 * Surcharge du thème original.
 * Modif: on enlève le "publié par" (byline).
 */
function libre_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	if ( is_single() ) {
		$posted_on = sprintf( esc_attr__( 'Posted on %1$s', 'libre' ),
						'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
					);
	}
	else {
		$posted_on = '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>';
	}

	echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'libre' ), esc_html__( '1 Comment', 'libre' ), esc_html__( '% Comments', 'libre' ) );
		echo '</span>';
	}

	if ( ! is_single() ) {
		edit_post_link( sprintf( esc_html__( 'Edit %1$s', 'libre' ), '<span class="screen-reader-text">' . the_title_attribute( 'echo=0' ) . '</span>' ), '<span class="edit-link">', '</span>' );
	}
}
